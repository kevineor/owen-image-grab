# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 17:26:15 2022

@author: owenc
"""

from vimba import *
import numpy as np
import matplotlib.pyplot as plt
import time

with Vimba.get_instance() as vimba:
    cams = vimba.get_all_cameras()
            
    with cams[0] as cam:
        frame = cam.get_frame()
        cam.TriggerMode.set('Off')
        
        while True:
            
            image = frame.as_numpy_ndarray()
            fig = plt.figure()
            plt.imshow(image)
            plt.colorbar()
            plt.savefig("image.png")
            plt.show(fig) #pourquoi on mets fig en argument et non image ?
            plt.close()
