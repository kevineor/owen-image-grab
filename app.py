# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 16:17:31 2022

@author: owenc
"""

from vimba import *
import sys
import math
import numpy as np
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QAction,\
QFileDialog, QLineEdit, QGridLayout, QGraphicsPixmapItem,\
QGraphicsView, QGraphicsScene, QSizePolicy
from PyQt5.QtGui import QIcon, QPixmap, QImage, QColor
from PyQt5 import QtCore

import time

#designer

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from scipy.optimize import curve_fit

from threading import Thread


class PlotCanvas(FigureCanvas):
 
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
 
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
 
        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.ax = self.figure.add_subplot(111)
        
class App(QMainWindow):

    def __init__(self):
        self.takingPictures = False
        super().__init__()
        self.initUI()
        self.offsetw=0
        self.offseth=0

    def takePicture(self):

        with Vimba.get_instance() as vimba:
            cams = vimba.get_all_cameras()
                    
            with cams[0] as cam:
                frame = cam.get_frame()
                cam.TriggerMode.set('Off')
                # numpy array as QImages
                image = frame.as_numpy_ndarray()
              
                fig = plt.figure(frameon=False)
                ax = plt.Axes(fig, [0., 0., 1., 1.])
                
                
             
                
                ax.set_axis_off()
                fig.add_axes(ax)
                
                #plt.imshow(image)
                
                ax.imshow(image, aspect='auto')
                fig.savefig("image.png")             
                plt.show()

                #plt.savefig("image.png")
                
                self.Image =QImage("image.png")
                #image2=np.ones((image.shape[0],image.shape[1],3),dtype=np.uint8)
                #image2 = np.transpose(image2, (1,0,2))
                
                #self.Image = QImage(image2, image2.shape[1], image2.shape[0],                                                                                                                                                 
                #     QImage.Format_RGB888)
                self.Display=self.Image.copy()
                self.update_display()  

                
    def myThread(self):
        while True:
            if self.takingPictures:
                self.takePicture()
                 
            time.sleep(0.005)
            
    def takeFrames(self, checked):
        self.takingPictures = checked
                
    
    def initUI(self):
        
        self.setWindowTitle('Image profiler')
        self.setGeometry(50, 50, 640, 480)        
        self.setWindowIcon(QIcon('resources/ELI_logo_vektoros_ok.png'))        
        self.widget = QWidget(self)
        
        
        # Create GraphicView display    
        self.view = QGraphicsView(self)
        self.view.setInteractive(True)
        self.view.setDragMode(QGraphicsView.RubberBandDrag)
        self.view.rubberBandChanged.connect(self.on_area_selection)
        self.prev_area=QtCore.QRect()
        
        # Adding right click menus
        self.view.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        unzoom_action = QAction("Unzoom", self)
        unzoom_action.triggered.connect(self.unzoom)        
        self.view.addAction(unzoom_action)
        
        
        # Create cross section graphs
        self.figw = PlotCanvas(self)
        self.figh = PlotCanvas(self)
        
        # Open file dialog menu
        openFile = QAction(QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showDialog)

        photo = QAction(QIcon('open.png'), 'Photo', self)
        photo.setStatusTip('grab frame')
        photo.triggered.connect(self.takePicture)     

        frames = QAction(QIcon('open.png'), 'Frames', self)
        frames.setStatusTip('grab frames')
        frames.triggered.connect(self.takeFrames)  
        frames.setCheckable(True)
        
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(openFile)
        fileMenu.addAction(photo)  
        fileMenu.addAction(frames)
        
        self.statusBar()
        
        # Create textedits
        self.text0=QLineEdit(self)
        self.text1=QLineEdit(self)
        self.text2=QLineEdit(self)
        self.text3=QLineEdit(self)
        self.text4=QLineEdit(self)
        self.text5=QLineEdit(self)
        self.text6=QLineEdit(self)
        self.text7=QLineEdit(self)
        self.value1=QLineEdit(self)
        self.value2=QLineEdit(self)
        self.value3=QLineEdit(self)
        self.value4=QLineEdit(self)
        self.value5=QLineEdit(self)
        self.value6=QLineEdit(self)
        self.value7=QLineEdit(self)
        self.value8=QLineEdit(self)        
        self.value9=QLineEdit(self)
        self.value10=QLineEdit(self)
        self.SG_order=QLineEdit(self)
        
        self.text0.setText('Super-gaussian order:')
        self.text1.setText('Width [pixel]')
        self.text2.setText('Height [pixel]')
        self.text3.setText('Image size:')          
        self.text4.setText('Center of mass:')
        self.text5.setText('FWHM:')
        self.text6.setText('Beam Diameter (1/e2):')
        self.text7.setText('Super Gaussian sigma:')

        
        self.value1.setText('0')
        self.value2.setText('0')
        self.value3.setText('0')
        self.value4.setText('0')
        self.value5.setText('0')
        self.value6.setText('0')
        self.value7.setText('0')
        self.value8.setText('0')
        self.value9.setText('0')
        self.value10.setText('0')
        self.SG_order.setText('1')
        
        self.text0.setReadOnly(True)
        self.text1.setReadOnly(True)
        self.text2.setReadOnly(True)
        self.text3.setReadOnly(True)
        self.text4.setReadOnly(True)
        self.text5.setReadOnly(True)
        self.text6.setReadOnly(True)
        self.text7.setReadOnly(True)
        
        self.value1.setEnabled(False)
        self.value2.setEnabled(False)
        self.value3.setEnabled(False)
        self.value4.setEnabled(False)
        self.value5.setEnabled(False)
        self.value6.setEnabled(False)
        self.value7.setEnabled(False)
        self.value8.setEnabled(False)
        self.value9.setEnabled(False)
        self.value10.setEnabled(False)
        
        # initialise grid2 layout
        
        grid2 = QGridLayout()
        
        grid2.addWidget(self.text0, 0, 0,1,2)
        grid2.addWidget(self.SG_order, 0, 2,1,1)
        grid2.addWidget(self.text1, 1, 1,1,1)
        grid2.addWidget(self.text2, 1, 2,1,1)         
        grid2.addWidget(self.text3, 2, 0,1,1)
        grid2.addWidget(self.text4, 3, 0,1,1)
        grid2.addWidget(self.text5, 4, 0,1,1)
        grid2.addWidget(self.text6, 5, 0,1,1)
        grid2.addWidget(self.text7, 6, 0,1,1)

        
        grid2.addWidget(self.value1, 2, 1,1,1)
        grid2.addWidget(self.value2, 2, 2,1,1)
        grid2.addWidget(self.value3, 3, 1,1,1)
        grid2.addWidget(self.value4, 3, 2,1,1)
        grid2.addWidget(self.value5, 4, 1,1,1)
        grid2.addWidget(self.value6, 4, 2,1,1) 
        grid2.addWidget(self.value7, 5, 1,1,1)
        grid2.addWidget(self.value8, 5, 2,1,1)
        grid2.addWidget(self.value9, 6, 1,1,1)
        grid2.addWidget(self.value10, 6, 2,1,1)
        
        # initialise grid layout
        grid = QGridLayout()
        grid.setSpacing(15)
        grid.setAlignment(QtCore.Qt.AlignTop)    
        grid.addWidget(self.view, 0, 0, 1, 1)
        grid.addWidget(self.figw, 1, 0, 1, 1)
        grid.addWidget(self.figh, 0, 1, 1, 1)
        grid.addLayout(grid2, 1, 1, 1, 1)
        grid.setColumnStretch(0, 2)
        grid.setRowStretch(0, 2)       
     
        self. widget.setLayout(grid)
        self.setCentralWidget(self.widget)
        
        # Change super-gaussian order
        self.SG_order.textChanged.connect(self.update_display)
       
        #   Display window 
        self.showMaximized()      
        self.show()
        
        self.mythread = Thread(target=self.myThread, daemon=True)
        self.mythread.start()
        
        
    def showDialog(self):

        fname = QFileDialog.getOpenFileName(self, 'Open file')
        
        if fname[0]:
            #pixmap = QPixmap(fname[0])            
            #self.Image = pixmap.toImage()    
            self.Image =QImage(fname[0])            
            self.Display=self.Image.copy()
            self.update_display()            
            
    def update_display(self):  
            
            Display2=self.Display.copy()
            #Display_gray=Display2.convertToFormat(QImage.Format_Grayscale8)            
            w=Display2.width()
            h=Display2.height()
            self.value1.setText(str(w))
            self.value2.setText(str(h))  
            
            Img = np.zeros((w,h))   # store 2-D data            self.text6.setText(str(np.size(Img,0)))                  
            
            l=0
            for i in range(w):
                for j in range(h):
                    Pixel=Display2.pixelColor(i,j)
                    Img[i,j] =Pixel.lightness()
                    l += Img[i,j]
    
           
            CMX = 0
            CMY = 0
            
            for i in range(w):
                for j in range(h):
                    CMX += Img[i,j]*i/l
                    CMY += Img[i,j]*j/l
            indw = round(CMX)
            indh = round(CMY)       
                  
            #ind = np.unravel_index(np.argmax(Img, axis=None), Img.shape)
            #indw=ind[0]
            #indh=ind[1]

            A=fwhm(Img[:,indh])          
            k1w=A[0]
            k2w=A[1]
            fwhmw=abs(k2w-k1w)
            A=sigma4(Img[:,indh])
            sigma4w=A[0]
            centw=A[1]
            
            A=fwhm(Img[indw,:])
            k1h=A[0]
            k2h=A[1]
            fwhmh=abs(k2h-k1h)
            A=sigma4(Img[indw,:])
            sigma4h=A[0]
            centh=A[1]
            
            mx=Img[indw,indh]
            minh=min(Img[indw,:])
            minw=min(Img[:,indh])
            
            self.value1.setText(str(w))
            self.value2.setText(str(h))            
            self.value3.setText(str(self.offsetw+indw))
            self.value4.setText(str(self.offseth+indh))
            self.value5.setText(str(fwhmw))
            self.value6.setText(str(fwhmh))
            self.value7.setText(str("{:.2f}".format(sigma4w)))
            self.value8.setText(str("{:.2f}".format(sigma4h)))
            
            # Fit super-gaussian

            Parw0 = np.array((1,centw,fwhmw))
            Parh0 = np.array((1,centh,fwhmh))
            try:
            
                poptw, pcovw= curve_fit(self.super_gaussian,range(w), Img[:,indh], Parw0, bounds=[(-np.inf, -np.inf, 0), (np.inf, np.inf, np.inf)])
                popth, pcovh= curve_fit(self.super_gaussian,range(h), Img[indw,:], Parh0, bounds=[(-np.inf, -np.inf, 0), (np.inf, np.inf, np.inf)])

                SGw=self.super_gaussian(range(w),*poptw)
                SGh=self.super_gaussian(range(h),*popth)
                self.value9.setText(str("{:.2f}".format(abs(poptw[2]))))
                self.value10.setText(str("{:.2f}".format(abs(popth[2]))))
                fit_ok=True
            except (RuntimeError):
                fit_ok=False
                self.value9.setText("Error")
                self.value10.setText("Error")



            
            for i in range(w):
                Display2.setPixelColor(i,indh,QColor(255, 0, 0))
                
            for j in range(h):
                Display2.setPixelColor(indw,j,QColor(255, 0, 0))                
            
            item=QGraphicsPixmapItem(QPixmap.fromImage(Display2))#image ne s'affiche pas si on enleve cette ligne
            scene=QGraphicsScene(self)
            scene.addItem(item)
         
            self.view.setScene(scene)#photo laser non affichée mais reste les courbes fitées si ligne enlevée
            self.view.fitInView(item,QtCore.Qt.IgnoreAspectRatio)
            self.view.show()

            
            self.figw.ax.clear()
            self.figw.ax.plot(range(w),Img[:,indh], 'k-')
            
            if fit_ok:
                self.figw.ax.plot(range(w),SGw, 'r--')   

            self.figw.ax.plot(indw,Img[indw,indh], 'b*')
            self.figw.ax.plot([k1w,k2w],[(mx-minw)/2+minw,(mx-minw)/2+minw], 'g-')
            self.figw.ax.plot([centw-sigma4w/2,centw+sigma4w/2],[(mx-minw)*math.exp(-2)+minw,(mx-minw)*math.exp(-2)+minw], 'g-')
            self.figw.draw()
            
            self.figh.ax.clear()
            self.figh.ax.plot(Img[indw,:],range(h), 'k-')
            
            if fit_ok:
                self.figh.ax.plot(SGh, range(h),'r--')  
                
            self.figh.ax.plot(mx,indh, 'b*')
            self.figh.ax.plot([(mx-minh)/2+minh,(mx-minh)/2+minh], [k1h,k2h],'g-')
            self.figh.ax.plot([(mx-minh)*math.exp(-2)+minh,(mx-minh)*math.exp(-2)+minh],[centh-sigma4h/2,centh+sigma4h/2], 'g-')

            self.figh.draw()
            
            
    def super_gaussian(self,x,C,x0,sigma):
        P= float(self.SG_order.text())
        
        return  C*((np.exp(-(2**(2*P-1))*np.log(2)*((  (x-x0)**2   )/ ((sigma)**2)   )**(P)))**2)        
    
        
        
    def on_area_selection(self, rubberBandRect, fromScenePoint, toScenePoint):
            
            if rubberBandRect.isNull():
                
                self.Display=self.Display.copy(self.x,self.y,self.width,self.height)
                self.offsetw=self.offsetw+self.x
                self.offseth=self.offseth+self.y
                self.update_display()
                
            P1=fromScenePoint
            P2=toScenePoint
            self.x=int(min([P1.x(),P2.x()]))
            self.y=int(min([P1.y(),P2.y()]))
            self.width=int(abs(P2.x()-P1.x()))
            self.height=int(abs(P2.y()-P1.y()))            
            
    def unzoom(self):
        self.Display=self.Image.copy()
        self.offsetw=0
        self.offseth=0
        self.update_display()       
        
            
def fwhm(x):
    N=len(x)
    
    # Get rid of background
    x=x-min(x)
    
    index_max = np.argmax(x)   
    k1=index_max
    while (x[k1]>x[index_max]/2)&(k1>0):
        k1=k1-1
            
    k2=index_max        
    while(x[k2]>x[index_max]/2)&(k2<N-1):
            k2=k2+1
          
    return (k1,k2)

def onepere2(x):
    N=len(x)
    
    # Get rid of background
    x=x-min(x)
    
    index_max = np.argmax(x)   
    k1=index_max
    while (x[k1]>x[index_max]*0.135)&(k1>0):
        k1=k1-1
            
    k2=index_max        
    while(x[k2]>x[index_max]*0.135)&(k2<N-1):
            k2=k2+1
          
    return (k2-k1)
  
def sigma4(x):
    N=len(x)
    ind=range(N)
    P=sum(x)
    cent=sum(x*ind)/P
    sigma2=sum(((ind-cent)**2)*x)/P
          
    return (4*math.sqrt(sigma2),cent)


     
if __name__ == '__main__':
    app = QApplication.instance()       
    if app is None:
        app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())