from vimba import *
import numpy as np
import matplotlib.pyplot as plt
import time

with Vimba.get_instance() as vimba:
    cams = vimba.get_all_cameras()
            
    with cams[0] as cam:
        frame = cam.get_frame()
        cam.TriggerMode.set('Off')
        image = frame.as_numpy_ndarray()
        fig = plt.figure()
        plt.imshow(image)
        plt.colorbar()
        plt.savefig("image.png")
        plt.show(fig) #pourquoi on mets fig en argument et non image ?
        plt.close()